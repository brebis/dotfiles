-- Install Packer automatically if it's not installed(Bootstraping)
-- Hint: string concatenation is done by `..`
local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end
local packer_bootstrap = ensure_packer()


-- Reload configurations if we modify plugins.lua
-- Hint
--     <afile> - replaced with the filename of the buffer being manipulated
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])


-- Install plugins here - `use ...`
return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Color scheme
    use "rafamadriz/neon"
    use "joshdick/onedark.vim"
    use ({ 'projekt0n/github-nvim-theme', tag = 'v0.0.7' })

    -- Conquer of Completion
    use {'neoclide/coc.nvim', branch = 'release'}
    use "IngoMeyer441/coc_current_word"

   -- Lualine
   use 'nvim-tree/nvim-web-devicons'
   use {'nvim-lualine/lualine.nvim',
   requires = {'nvim-tree/nvim-web-devicons', opt = true}
}

    -- NerdTree and all related plugins
    use 'preservim/nerdtree'
    use 'ryanoasis/vim-devicons'


    -- TagBar
    use "preservim/tagbar"

    -- Git gutter
    use "airblade/vim-gitgutter"

    -- Barbar (tabline)
    use {'romgrk/barbar.nvim', requires = 'nvim-web-devicons'}

    -- Clang Format
    use "rhysd/vim-clang-format"

    -- Alpha Nvim
    use {'goolord/alpha-nvim',
    requires = {'nvim-tree/nvim-web-devicons'},
    config = function ()
        require'alpha'.setup(require'alpha.themes.startify'.config)
    end
}

    -- Css colorization
    use "norcalli/nvim-colorizer.lua"

end)
