-- Hint: use `:h <option>` to figure out the meaning if needed
-- vim.opt.clipboard = 'unnamedplus'   -- use system clipboard 
-- vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
-- vim.opt.mouse = 'a'                 -- allow the mouse to be used in Nvim

-- Tab
vim.opt.tabstop = 4                 -- number of visual spaces per TAB
vim.opt.softtabstop = 4             -- number of spacesin tab when editing
vim.opt.shiftwidth = 4              -- insert 4 spaces on a tab
vim.opt.expandtab = true            -- tabs are spaces, mainly because of python

-- UI config
vim.opt.number = true               -- show absolute number
vim.opt.relativenumber = true       -- add numbers to each line on the left side
vim.opt.cursorline = true           -- highlight cursor line underneath the cursor horizontally
vim.opt.splitbelow = true           -- open new vertical split bottom
vim.opt.splitright = true           -- open new horizontal splits right
vim.opt.termguicolors = true        -- enabl 24-bit RGB color in the TUI
vim.opt.showmode = false            -- we are experienced, wo don't need the "-- INSERT --" mode hint
vim.opt.colorcolumn = "80"

-- Characters config
vim.cmd([[set list listchars=tab:>-,eol:¬]])

-- Searching
vim.opt.incsearch = true            -- search as characters are entered
vim.opt.hlsearch = false            -- do not highlight matches
vim.opt.ignorecase = true           -- ignore case in searches by default
vim.opt.smartcase = true            -- but make it case sensitive if an uppercase is entered

-- Clang Format
-- Enable for c and c++ language
vim.cmd([[autocmd FileType c,h,cpp,hpp ClangFormatAutoEnable]])

-- Conquer of Completion
-- Set current word highlighting delay to 250 ms
vim.cmd([[let g:coc_current_word_highlight_delay = 250]])

-- NerdTree
-- Automatically delete the buffer of the file you just deleted with NerdTree
vim.cmd([[let NERDTreeAutoDeleteBuffer = 1]])
-- Close Vim or a tab automatically when NERDTree is the last window
vim.cmd([[autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif]])
-- Add arrows to directory link
vim.cmd([[let NERDTreeDirArrows = 1]])
vim.cmd([[let g:NERDTreeDirArrowExpandable = '']])
vim.cmd([[let g:NERDTreeDirArrowCollapsible = '']])
