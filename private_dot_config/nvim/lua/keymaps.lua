-- Hint: see `:h vim.map.set()`
-- define common options
local opts = {
    noremap = true,      -- non-recursive
    silent = true,       -- do not show message
}

-------------------------------------------------------------------------------
--------------------------- Normal mode ---------------------------------------
-------------------------------------------------------------------------------

-- Basic keymaps
vim.keymap.set('n', '<space>', ':', opts)
vim.keymap.set('n', 'o', 'o<esc>', opts)
vim.keymap.set('n', 'O', 'O<esc>', opts)

-- Better window navigation
vim.keymap.set('n', '<C-Up>', '<C-w>h', opts)
vim.keymap.set('n', '<C-Down>', '<C-w>j', opts)
vim.keymap.set('n', '<C-Left>', '<C-w>k', opts)
vim.keymap.set('n', '<C-Right>', '<C-w>l', opts)

-- Resize with arrows
-- delta: 2 lines
vim.keymap.set('n', '<C-r>Up', ':resize -2<CR>', opts)
vim.keymap.set('n', '<C-r>Down', ':resize +2<CR>', opts)
vim.keymap.set('n', '<C-r>Left', ':vertical resize -2<CR>', opts)
vim.keymap.set('n', '<C-r>Right', ':vertical resize +2<CR>', opts)


-- Barbar keymaps -------------------------------------------------------------
local map = vim.api.nvim_set_keymap

-- Goto buffer in position...
map('n', 'b1', '<Cmd>BufferGoto 1<CR>', opts)
map('n', 'b2', '<Cmd>BufferGoto 2<CR>', opts)
map('n', 'b3', '<Cmd>BufferGoto 3<CR>', opts)
map('n', 'b4', '<Cmd>BufferGoto 4<CR>', opts)
map('n', 'b5', '<Cmd>BufferGoto 5<CR>', opts)
map('n', 'b6', '<Cmd>BufferGoto 6<CR>', opts)
map('n', 'b7', '<Cmd>BufferGoto 7<CR>', opts)
map('n', 'b8', '<Cmd>BufferGoto 8<CR>', opts)
map('n', 'b9', '<Cmd>BufferGoto 9<CR>', opts)
map('n', 'b0', '<Cmd>BufferLast<CR>', opts)

-- Close buffer
map('n', 'bc', '<Cmd>BufferClose<CR>', opts)


-- NerdTree
vim.keymap.set('n', 'nn', '<Cmd>NERDTreeToggle <CR>', opt)

-- Tag Bar --------------------------------------------------------------------
vim.keymap.set('n', 'tc', '<Cmd>TagbarClose <CR>', opt)
vim.keymap.set('n', 'tt', '<Cmd>TagbarOpen <CR>', opt)


-------------------------------------------------------------------------------
--------------------------- Visual mode ---------------------------------------
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
--------------------------- Insert mode ---------------------------------------
-------------------------------------------------------------------------------

-- Simple escape
vim.keymap.set('i', 'ii', '<esc>', opts)


-- Conquer of completion ------------------------------------------------------
-- Use tab and s-tab to navigate the completion list
vim.cmd([[inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"]])
vim.cmd([[inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"]])

-- Confirm the completion
vim.cmd([[inoremap <silent><expr> <cr> coc#pum#visible() && coc#pum#info()['index'] != -1 ? coc#pum#confirm() : "\<C-g>u\<CR>"]])
