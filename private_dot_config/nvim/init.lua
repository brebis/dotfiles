require('options')
require('keymaps')
require('plugins')
require('colorscheme')

-- Launch plugins
-- Github color theme
require("github-theme").setup({
  theme_style = "dark",
  function_style = "italic",

  -- Change the "hint" color to the "orange" color, and make the "error" color bright red
  colors = {hint = "orange", error = "#aa0000"},
})

-- Lualine
require('lualine').setup{
    options = {theme = 'modus-vivendi'}
}

-- Colorizer
require('colorizer').setup()
